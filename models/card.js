var db = require('../lib/db');

var cardSchema = new db.Schema({
	carName : {type: String, unique: true},
	year	: Number,
	factory : String,
});

var newCar = db.mongoose.model('Card', cardSchema);

//Exports
module.exports.addCard		= addCard;
module.exports.listCards	= listCards;
module.exports.removeCard	= removeCard;
module.exports.updateCard	= updateCard;
module.exports.editCard		= editCard;

function addCard(carName, year, factory, callback) {
	var instance		= new newCar();
	instance.carName	= carName;
	instance.year		= year;
	instance.factory	= factory;
	instance.save(function(err){
		if(err){
			callback(err);
		} else {
			callback(null, instance);
		}
	});
}

function updateCard(idCard, carName, year, factory, callback) {
	var options = { multi: true };
	var instance = newCar.update({'_id': Object(idCard)},{ $set: { carName: carName, year: year, factory: factory }}, options, callback);
}

function listCards(callback) {
	var instance = newCar.find(function(err, data){
		callback(null, data);
	});
}

function editCard(idCard, callback) {
	var instance = newCar.find({'_id': Object(idCard)}, function(err, data){
		callback(null, data);
	});
}

function removeCard(idCard, callback) {
	var instance = newCar.remove({'_id': Object(idCard)}, function(err, data){
		callback(null, data);
	});
}
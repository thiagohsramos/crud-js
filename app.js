var express = require('express'),
	fs		= require('fs'),
	routes	= require('./routes'),
	Card	= require('./models/card.js'),
	jade	= require('jade'),
	Bliss	= require('bliss'),
	bliss	= new Bliss();

var app = express();

// Configuration
app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

app.get('/', function(req, res){
	res.type('text/html', 'utf-8');
	var resp = {
		'title': 'T&iacute;tulo retornado de obj utilizando Bliss Node template',
		'body': 'Propriedade body retornada do JSON obj resp - Bliss Node template'
	};

	res.send(bliss.render('views/layout', resp));
});

app.get('/register-card/', function(req, res){
	res.render('register-cards');
});

app.post('/addCard/', function(req, res) {
	var carName = req.body.carName;
	var year = req.body.year;
	var factory = req.body.factory;
	if (carName == '' || year == ''|| factory == '') {
          res.render('register-cards', {'msg': 'preencha corretamente os campos solicitados.'} );
	} else if (isNaN(year)) {
	  res.render('register-cards', {'msg': 'O campo Ano deve ser numerico, preencha-o corretamente e tente de novo.'} );
	} else {
	  
	  Card.addCard(carName, year, factory, function(err, car) {
	    if (err) throw err;
	    res.redirect('/register-card/');
	  });
	}
});

app.post('/updateCard/:id', function(req, res) {
	var carName = req.body.carName;
	var year = req.body.year;
	var factory = req.body.factory;
	if (carName == '' || year == ''|| factory == '') {
          res.render('update-card', {'msg': 'preencha corretamente os campos solicitados.'} );
	} else if (isNaN(year)) {
	  res.render('update-card', {'msg': 'O campo Ano deve ser numerico, preencha-o corretamente e tente de novo.'} );
	} else {
	  Card.updateCard(req.params.id, carName, year, factory, function(err, car) {
	    if (err) throw err;
	    res.redirect('/listCard/');
	  });
	}
});

/* Listar cartas cadastradas */
app.get('/listCard/', function(req, res){
	Card.listCards(function(err, cars){
		res.render('list-cars', {'cars': cars});
	});
});

app.get('/editCard/:id', function(req, res){
	Card.editCard(req.params.id,function(err, cars){
		res.render('update-card', {'caredit': JSON.stringify(cars)} );
	});
});

app.get('/removeCard/:id', function(req, res){
	Card.removeCard(req.params.id,function(err, cars){
		res.redirect('/listCard/');
	});
});


var server = app.listen(8071, function(){
	console.log("Express server listening on port %d in %s mode", server.address().port, app.settings.env);
});
